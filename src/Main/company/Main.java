package Main.company;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

public class Main {

    public static void main(String[] args) {
        String quary = "http://httpbin.org/post";

        HttpURLConnection connection = null;

        try {
            connection = (HttpURLConnection) new URL(quary).openConnection();

            connection.setRequestMethod("POST");
            connection.setUseCaches(false);
            connection.setConnectTimeout(300);

            connection.connect();

            StringBuilder bld = new StringBuilder();

            if(HttpURLConnection.HTTP_OK == connection.getResponseCode()){
                BufferedReader buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String line;
                while ((line = buffer.readLine()) != null){
                    bld.append(line);
                    bld.append("\n");
                }

                System.out.println(bld.toString());
            }else {
                System.out.println("Fail: " + connection.getResponseCode() + ", " + connection.getResponseMessage());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (connection !=null){
                connection.disconnect();
            }
        }
    }
}
